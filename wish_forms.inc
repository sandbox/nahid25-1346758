<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rabit
 * Date: 10/3/11
 * Time: 4:58 PM
 * To change this template use File | Settings | File Templates.
 */
 


/**
* Form Description 
*
* return array
*/

function qm_wish_form($form, &$form_state, $wid = NULL) {
    
	$wish = "";	
   	if (isset($wid)) {
        	$wid_edit= db_select('qm_wishes', 'w')
            ->fields('w', array('wid','wish'))
            ->condition('wid',$wid)
            ->execute();

		while($result = $wid_edit->fetchAssoc()) {
			$wish = $result['wish'];	
		}

		$form['wid'] = array('#type' => 'hidden', '#value' => $wid);
   	
  	}      

		$form['value_wishes'] = array(
			'#title' => t('Enter your wishes here.'),
			'#type' => 'textarea',
			'#rows' => 2,
			'#cols' =>30,
		    '#default_value' => $wish,
		);	

		$form['submit'] = array(
		    '#type' => 'submit',
		    '#value' => t('Submit')
		);	

		return $form;

    }


/**
 *
 * Implements hook_submit
 *
 * @param $form_id
 * @param $form_state
 * @return void
 */
function qm_wish_form_submit($form, &$form_state){


    $action = (isset($form_state['values']['wid']))?'edit':'add';

    switch ($action){

        case 'add':

            global $user;

            $creation_time = REQUEST_TIME;

            $wid = db_insert('qm_wishes')
                    ->fields(array(
                                  'user' => $user->uid,
                                  'created' =>$creation_time,
                                  'language' =>'en',
                                  'wish' => $form_state['values']['value_wishes'],
                                  'status' => 0,
                                  'count' => 0,
                             ))
                    ->execute();

            drupal_set_message(t('Your wish has been submitted for moderation.'));

            break;
        
        case 'edit':

            $wid = $form_state['values']['wid'];

            $edited = db_update('qm_wishes')
                    ->fields(array(
                                  'wish' => $form_state['values']['value_wishes'],
                                  //                              'status' => 0,
                             ))
                    ->condition('wid', $wid)
                    ->execute();


            drupal_set_message(t('Edited and Submitted successfully '). $wid);

            $path = 'admin/config/wishes';
            drupal_goto($path);

            break;
    }


//echo $wid;

}





