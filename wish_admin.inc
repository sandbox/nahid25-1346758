<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rabit
 * Date: 10/3/11
 * Time: 4:58 PM
 * To change this template use File | Settings | File Templates.
 */
 


function wish_admin_page() {
	
	$output = "<h2>Wishes Admin Panel.</h2>";

	$query = db_select('qm_wishes', 'w')
            ->fields('w', array('wid','user','created', 'wish', 'status'));
	$query->addJoin('LEFT','users', 'u', 'w.user = u.uid'); // join field table
    $query->addField('u', 'name'); // add field
    $query->orderBy('wid', 'DESC');
    $query->condition('w.status', 0);
	$result = $query->execute();
	
	$output.= "<table>";
	$output.= "<tr><th>Wid</th><th>User</th><th>Wishes</th><th>Edit</th><th>Confirm</th><th>Delete</th></tr>";
	while($record = $result->fetchAssoc()) {
		$output.= "<tr>";
        $output.= "<td>".$record['wid']."</td>";
        $output.= "<td>".$record['name']."</td>";
        $output.= "<td>".$record['wish']."</td>";
        $output.= "<td>".l(("Edit"),"admin/config/wishes/edit/".$record['wid'])."</td>";
        $output.= "<td>".l(("Publish"),"admin/config/wishes/publish/".$record['wid'])."</td>";
        $output.= "<td>".l(("Delete"),"admin/config/wishes/delete/".$record['wid'])."</td>";
        $output.= "</tr>";
    }
    $output.= "</table>";

	return $output;
}



/* Delete wishes from admin panel

*/

function wish_delete_page($wid) {

    $wid_deleted = db_delete('qm_wishes')

            ->condition('wid',$wid)

            ->execute();

    drupal_set_message(t('Deleted successfully '). $wid);

    $path = 'admin/config/wishes';
     drupal_goto($path);
}


/* Publish wishes from admin panel

*/

function wish_publish_page($wid) {

    $wid_published = db_update('qm_wishes')
            ->fields(array(
                          'status' => 1
                     ))

            ->condition('wid',$wid)

            ->execute();

    drupal_set_message(t('Published successfully '). $wid);

    $path = 'admin/config/wishes';
    drupal_goto($path);

}








